#!/bin/bash
set -ex

[[ -d ${0%/*} ]] && cd "${0%/*}"/../

source /etc/os-release

cd tests

# fedpkg fetch sources based on branch.f$VERSION_ID.remote
if ! git remote show | grep -q fedora_src; then
	git remote add fedora_src https://src.fedoraproject.org/rpms/kexec-tools.git
	git config --add branch.f$VERSION_ID.remote fedora_src
fi

BASE_IMAGE=/usr/share/cloud_images/Fedora-Cloud-Base-35-1.2.x86_64.qcow2 RELEASE=$VERSION_ID make test-run
